# Box Away

How to optimize the boxing process.

__Java Source__
This folder contains the source code for the algorithm written in java, we rewrote the algorithm in rust as we quickly ran into memory limitation while using java that were not solved by increasing memory allocation. The rewrite into rust uses the exact same algorithm with it being as close to a clone as was feasibly possible (naming conventions changed to match languages).

__Rust Source__
To run the source code of this project all that should be needed is to clone this repository and to run 'cargo run' within the directory. This will download all required packages and should start running. There are also a set of linux binaries if you would prefer to just run the project and not build it yourself. We running the built and optimized version on Windows, the exe's that were generated would consistency hit stack overflows that were not observed on Linux. As such we have only included the binaries which can be found under the releases folder or tab in the sidebar. Also if one is trying to build on Windows the cargo.lock file may cause issues, just delete the file and rebuild the project with 'cargo build'. If you would like to try to build the optimized version of the code use 'cargo build --release' which will place a binary/exe in the target/release folder of the project.

Please note that the included binaries have most logging removed. They are only for performance testing and do not display any form of information about what items are in what boxes.