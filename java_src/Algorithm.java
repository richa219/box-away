import java.util.ArrayList;

public class Algorithm {
    
    /**
     * Runs the packaging algorithm. Try's to sort all given passed items into some form of the passed box sizes.
     * @param boxSizes The list of sample boxes that items are able to be placed within, these are assumed to be ordered from smallest to largest volume.
     * @param items The list of items that are to be packaged, these are assumed to be ordered from smallest to largest volume.
     * @param packagedBoxes The list that will contain all packaged boxes with items. This is originally passed an empty list that will be filled once the method finishes.
     * @return If not all items are able to be placed into boxes this method will return false, otherwise if there is a configuration where all items can be placed within boxes the method will return true.
     */
    public static boolean Package(ArrayList<Box> boxSizes, ArrayList<Item> items, ArrayList<Box> packagedBoxes) {
        double totalItemVolume = 0;
        double totalItemWeight = 0;
        for (int n = 0; n < items.size(); n++) {
            Item i = items.get(n);
            // if (!ItemDimensionVerification(i, boxSizes.get(boxSizes.size() - 1)) || i.weight > boxSizes.get(boxSizes.size() - 1).weightLimit) {
            //     return false; // There exists an item that is not physically capable of being placed within the box
            // }
            totalItemVolume += i.volume;
            totalItemWeight += i.weight;
        }
        // Try to insert all remaining items into a single box
        for (Box bSize : boxSizes) {
            if (totalItemVolume <= bSize.volume && totalItemWeight <= bSize.weightLimit) {
                // All remaining items fit within a single box and are added to the box.\
                Box lastBox = new Box(bSize);
                for (Item i : items) {
                    lastBox.add(i);
                }
                packagedBoxes.add(lastBox);
                return true;
            }
        }

        // If a single box cannot fit every item passed to the method
        // Get the largest box, place as many items as possible into it, trying to place items within it starting from largest to smallest volume
        // This currently adds items from smallest to largest but the code in the rust version properly adds from largest to smallest
        Box incrementalBox = new Box(boxSizes.get(boxSizes.size() - 1));
        for (int n = 0; n < items.size(); n++) {
            // Adds the item if possible to the box and removes it from the list of items.
            if (incrementalBox.add(items.get(n))) {
                items.remove(n);
            }
        }
        // Add the box to the list of packaged boxes
        packagedBoxes.add(incrementalBox);
        return Package(boxSizes, items, packagedBoxes);
    }
}