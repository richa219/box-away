public class Item implements Comparable<Item> {
    public double weight;
    public double height;
    public double length;
    public double width;
    public double volume;

    public Item (double weight, double height, double length, double width) {
        this.weight = weight;
        this.height = height;
        this.length = length;
        this.width = width;
        this.volume = height * length * width;
    }

    @Override
    public int compareTo(Item i) {
        if (this.volume < i.volume) {
            return -1;
        } else if (this.volume > i.volume) {
            return 1;
        } else {
            if (this.weight < i.weight) {
                return -1;
            } else if (this.weight > i.weight) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
}