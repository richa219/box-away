import java.util.*;

public class Testing {
    public static void main(String[] args) {
        Random rand = new Random();
        long avg = 0;
        double numBoxes = 0;
        for (int z = 0; z < 100; z++) {
            // Test 1: All items can fit within a single box small
            ArrayList<Box> sizes = new ArrayList<>();
            ArrayList<Item> sampleItems = new ArrayList<>();
            ArrayList<Box> packagedBoxes = new ArrayList<>();
            // Create box sizes
            for (int n = 0; n < 3; n++) {
                double weight = (double)(4000 + rand.nextInt(8000 + 1 - 4000)) / 10;
                double height = (double)(4000 + rand.nextInt(8000 + 1 - 4000)) / 10;
                double length = (double)(4000 + rand.nextInt(8000 + 1 - 4000)) / 10;
                double width = (double)(4000 + rand.nextInt(8000 + 1 - 4000)) / 10;
                sizes.add(new Box(weight, height, length, width));
            }
            // Get largest volume
            // Box worst = sizes.get(0);
            // for (Box b : sizes) {
            //     if (b.volume > worst.volume) {
            //         worst = b;
            //     }
            // }
            // Create items
            // for (int n = 0; n < 100; n++) {
            //     double weight = worst.weightLimit / 1.1;
            //     double height = worst.height / 2;
            //     double length = worst.length / 1.1;
            //     double width = worst.width / 1.1;
            //     sampleItems.add(new Item(weight, height, length, width));
            // }
            for (int n = 0; n < 100000; n++) {
                double weight = (double)(rand.nextInt(401)) / 10;
                double height = (double)(rand.nextInt(101)) / 10;
                double length = (double)(rand.nextInt(101)) / 10;
                double width = (double)(rand.nextInt(101)) / 10;
                sampleItems.add(new Item(weight, height, length, width));
            }
            // Run and do performance testing
            avg += processData(sizes, sampleItems, packagedBoxes);
            numBoxes += packagedBoxes.size();
        }
        avg = avg / 100;
        numBoxes = numBoxes / 100;
        System.out.println(avg + "ns, " + numBoxes + " boxes.");
    }

    public static long processData(ArrayList<Box> boxSizes, ArrayList<Item> items, ArrayList<Box> packagedBoxes) {
        // int itemCount = items.size();
        // int boxCount = boxSizes.size();
        long startTime = System.nanoTime();
        Algorithm.Package(boxSizes, items, packagedBoxes);
        long endTime = System.nanoTime();
        long elapsed = endTime - startTime;
        // System.out.println("Took: " + elapsed + "ms with " + itemCount + " items and " + boxCount + " boxes.");
        // System.out.println("The algorithm placed the items into " + packagedBoxes.size() + " boxes.");
        // for (int n = 0; n < packagedBoxes.size(); n++) {
        //     System.out.println("Box " + (n + 1) + " has volume: " + packagedBoxes.get(n).volume + ", weight capacity: " + packagedBoxes.get(n).weightLimit + ", and contains:");
        //     for (int i = 0; i < packagedBoxes.get(n).items.size(); i++) {
        //         Item item = packagedBoxes.get(n).items.get(i);
        //         System.out.println("\tItem " + (i + 1) + ", Volume: " + item.volume + ", Height: " + item.height + ", Length: " + item.length + ", Width: " + item.width + ", Weight: " + item.weight);
        //     }
        // }
        // if (packagedBoxes.size() == 0) {
        //     System.out.println("Here");
        // }
        return elapsed;
    }
}