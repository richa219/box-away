import java.util.ArrayList;

public class Box {
    public double weightLimit;
    public double height;
    public double length;
    public double width;
    public double volume;
    public double remainingVolume;
    public double remainingWeight;
    ArrayList<Item> items;

    public Box (double weightLimit, double height, double length, double width) {
        this.weightLimit = weightLimit;
        this.height = height;
        this.length = length;
        this.width = width;
        this.volume = height * length * width;
        this.remainingVolume = this.volume;
        this.remainingWeight = this.weightLimit;
        this.items = new ArrayList<Item>();
    }

    public Box (Box ref) {
        this.weightLimit = ref.weightLimit;
        this.height = ref.height;
        this.length = ref.length;
        this.width = ref.width;
        this.volume = ref.volume;
        this.remainingVolume = this.volume;
        this.remainingWeight = this.weightLimit;
        this.items = new ArrayList<Item>();
    }

    public boolean add(Item i) {
        if (i.weight > this.remainingWeight) {
            return false;
        }
        this.remainingVolume -= i.volume;
        this.remainingWeight -= i.weight;
        return this.items.add(i);
    }
}