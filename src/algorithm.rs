pub struct Item {
    pub volume: f64,
    pub weight: f64,
    pub height: f64,
    pub length: f64,
    pub width: f64
}

impl Item {
    pub fn new(weight: f64, height: f64, length: f64, width : f64) -> Item {
        Item {
            volume: (height * length * width),
            weight: weight,
            height: height,
            length: length,
            width: width,
        }
    }

    // This is another constructor 
    pub fn clone(other: &Item) -> Item {
        Item {
            volume: other.volume,
            weight: other.weight,
            height: other.height,
            length: other.length,
            width: other.width
        }
    }
}

/// In the Java code this is the Box object, Box is a reserved word in Rust
pub struct Container {
    pub weight_limit: f64,
    height: f64,
    length: f64,
    width: f64,
    pub volume: f64,
    remaining_volume: f64,
    remaining_weight: f64,
    pub items: Vec<Item>,
}

impl Container {
    pub fn new(weight_limit: f64, height: f64, length: f64, width: f64) -> Container {
        Container {
            weight_limit: weight_limit,
            height: height,
            length: length,
            width: width,
            volume: height * length * width,
            remaining_volume: height * length * width,
            remaining_weight: weight_limit,
            items: Vec::new(),
        }
    }

    /// This is another constructor and should be called clone
    pub fn from_ref(other: &Container) -> Container {
        Container {
            weight_limit: other.weight_limit,
            height: other.height,
            length: other.length,
            width: other.width,
            volume: other.volume,
            remaining_volume: other.volume,
            remaining_weight: other.weight_limit,
            items: Vec::new(),
        }
    }

    /// Tries to add an Item to the Container/Box
    pub fn add(&mut self, i: &Item) -> bool {
        if i.weight > self.remaining_weight {
            return false;
        } else {
            self.remaining_volume -= i.volume;
            self.remaining_weight -= i.weight;
            self.items.push(Item::clone(i));
            return true;
        }

    }
}

/// Our packaging algorithm.
/// 
/// __box_sizes__ is a list of Containers that is sorted from smallest to largest volume
/// 
/// __items__ is a list of Items that are to be packaged
/// 
/// __packaged_boxes__ is a list of Containers that starts empty but once the function finishes should contain Containers that will each have some or all of the Items from __items__
pub fn package(box_sizes: &Vec<Container>, mut items: Vec<Item>, mut packaged_boxes: Vec<Container>) -> Option<Vec<Container>> {
    let mut total_item_volume: f64 = 0.0;
    let mut total_item_weight: f64 = 0.0;
    for n in 0..items.len() {
        let i: &Item = items.get(n).unwrap();
        total_item_volume += i.volume;
        total_item_weight += i.weight;
    }
    // Try to place all items into a single box, go through each box size
    for b_size in box_sizes {
        // If the total item weight and volume would fit into a box then go ahead
        if total_item_volume <= b_size.volume && total_item_weight <= b_size.weight_limit {
            let mut last_box: Container = Container::from_ref(&b_size);
            // Add items to a container/box
            for i in &items {
                last_box.add(&Item::clone(i));
            }
            // Add the filled box to the list of packaged boxes
            packaged_boxes.push(last_box);
            return Some(packaged_boxes);
        }
    }
    let mut incremental_box : Container = Container::from_ref(box_sizes.get(box_sizes.len() - 1).unwrap());
    let mut n = items.len();
    // Add some as many remaining items into the largest box
    while n > 0 { 
        if incremental_box.add(items.get(n - 1).unwrap()) {
            items.remove(n - 1);
        }
        n = n - 1;
    }
    // Add the now filled container to the list of finished boxes and start over with the remaining items
    packaged_boxes.push(incremental_box);
    return package(&box_sizes, items, packaged_boxes);
}