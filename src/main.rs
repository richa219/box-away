// The commented lines of code throughout this file are optional logging that can be enabled. Those starting with 'A' represent lines that enable logging data into a csv file while those with 'B' add details about the contents of the containers.
mod algorithm;
use rand::prelude::*;
// use csv::Writer;
use serde::Serialize;
use std::time::{Duration, Instant};

fn main() {
    // A: let mut wtr = Writer::from_path("performance.csv").unwrap();
    let mut rng = rand::thread_rng();
    let mut _total_time: u128 = 0;
    let mut _total_boxes: f64 = 0.0;
    let loops = 1;
    // Determines the number of items to generate.
    let _items: u64 = 100;
    let boxes: u64 = 3;
    // A: let mut x = 10;
    // A: while x <= 10000 {
        _total_time = 0;
        _total_boxes = 0.0;
        for _z in 0..loops {
            let mut sizes: Vec<algorithm::Container> = Vec::new();
            let mut sample_items: Vec<algorithm::Item> = Vec::new();
            let packaged_boxes: Vec<algorithm::Container> = Vec::new();
    
            // Generate the various box sizes
            for n in 0..boxes {
                let upper_limit_weight: f64 = 200.0;
                let lower_limit_weight: f64 = 50.0;
                let upper_limit: f64 = 200.0;
                let lower_limit: f64 = 50.0; 
                let weight: f64 = rng.gen::<f64>() * (upper_limit_weight - lower_limit_weight) + lower_limit_weight;
                let height: f64 = rng.gen::<f64>() * (upper_limit - lower_limit) + lower_limit;
                let length: f64 = rng.gen::<f64>() * (upper_limit - lower_limit) + lower_limit;
                let width: f64 = rng.gen::<f64>() * (upper_limit - lower_limit) + lower_limit;
                sizes.push(algorithm::Container::new(weight, height, length, width));
                // sizes.push(algorithm::Container::new(100.0, 50.0, 50.0, 50.0));
            }
            // Generate the various items
            // A: for _n in 0..x {
            for _n in 0.._items {
                let upper_limit_weight: f64 = 0.0;
                let lower_limit_weight: f64 = 55.0;
                let upper_limit: f64 = 30.0;
                let lower_limit: f64 = 20.0;
                let weight: f64 = rng.gen::<f64>() * (upper_limit_weight - lower_limit_weight) + lower_limit_weight;
                let height: f64 = rng.gen::<f64>() * (upper_limit - lower_limit) + lower_limit;
                let length: f64 = rng.gen::<f64>() * (upper_limit - lower_limit) + lower_limit;
                let width: f64 = rng.gen::<f64>() * (upper_limit - lower_limit) + lower_limit;
                sample_items.push(algorithm::Item::new(weight, height, length, width));
            }
            let results = process_data(sizes, sample_items, packaged_boxes);
            _total_time += results.0.as_nanos();
            _total_boxes += results.1;
        }
        let avg_time = _total_time / loops as u128;
        let avg_boxes = _total_boxes / loops as f64;
        println!("FINAL STATISTICS:\nTest Took: {}ns = {}ms = {}s for {} tests.\nAverage Time: {}ns, Average Number of Boxes: {}.", _total_time, _total_time / u128::pow(10, 6) , _total_time / u128::pow(10, 9), loops, avg_time, avg_boxes);
        // A: wtr.serialize(Row {
        // A:     items: x,
        // A:     boxes: boxes,
        // A:     avg_time: avg_time,
        // A:     avg_boxes: avg_boxes
        // A: }).unwrap();
        // A: println!("{}",x);
        // A: x = x + 100;
    // }
}

// Gets the time to run for an instance of the algorithm
fn process_data(box_sizes: Vec<algorithm::Container>, items: Vec<algorithm::Item>, packaged_boxes: Vec<algorithm::Container>) -> (Duration, f64) {
    let _item_count = items.len();
    let start = Instant::now();
    let result = algorithm::package(&box_sizes, items, packaged_boxes);
    let duration = start.elapsed();
    let final_packages = result.unwrap();
    println!("Time: {}ns, Box Sizes: {}, Items: {}, Final Count: {}", duration.as_nanos(), box_sizes.len(), _item_count, final_packages.len());
    // B: println!("Took: {}ns with {} items and {} boxes.", duration.as_nanos(), _item_count, box_sizes.len());
    // B: println!("The algorithm  placed the items into {} boxes.", final_packages.len());
    // B: for n in 0..final_packages.len() {
    // B:     println!("Box {} has volume {}, weight capacity {}, and contains:", n + 1, final_packages.get(n).unwrap().volume, final_packages.get(n).unwrap().weight_limit);
    // B:     for i in 0..final_packages.get(n).unwrap().items.len() {
    // B:         let item: &algorithm::Item = final_packages.get(n).unwrap().items.get(i).unwrap();
    // B:         println!("\tItem {}, Volume: {}, Height: {}, Length: {}, Width: {}, Weight: {}", i + 1, item.volume, item.height, item.length, item.width, item.weight);
    // B:     }
    // B: }
     
    return (duration, final_packages.len() as f64)
}

// This is a data type to help store our results into a csv file
#[derive(Serialize)]
struct Row {
    items: u64,
    boxes: u64,
    avg_time: u128,
    avg_boxes: f64
}